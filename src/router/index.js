import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'event-list',
    component: () => import('../views/EventList.vue'),
  },
  {
    path: '/event',
    name: 'event-show',
    component: () => import('../views/EventShow.vue'),
  },
  {
    path: '/event/create',
    name: 'event-create',
    component: () => import('../views/EventCreate.vue'),
  },
  {
    path: '/users/:iduser',
    name: 'users',
    component: () => import('../views/Users.vue'),
    props: true,
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
