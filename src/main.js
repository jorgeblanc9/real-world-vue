import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'
import 'fontawesome-4.7/css/font-awesome.min.css'

// https://v3.vuejs.org/cookbook/automatic-global-registration-of-base-components.html#base-example

const app = createApp(App).use(store).use(router)

const requireComponent = require.context(
  './components/base',
  false,
  /Base[A-Z]\w+\.(vue|js)$/
)

requireComponent.keys().forEach((fileName) => {
  const componentConfig = requireComponent(fileName)

  const componentName = upperFirst(
    camelCase(
      fileName
        .split('/')
        .pop()
        .replace(/\.\w+$/, '')
    )
  )

  app.component(componentName, componentConfig.default || componentConfig)
})

app.mount('#app')
